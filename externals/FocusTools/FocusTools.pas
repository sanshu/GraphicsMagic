{ From: http://www.codebot.org/delphi/?doc=9502 }

unit FocusTools;

interface

uses
	Windows, Messages, SysUtils, Classes, Graphics;

{ TBasicWindow class }

type
  TCreateInfo = record
  	X, Y, W, H: Integer;
    Style: Cardinal;
    ExStyle: Cardinal;
    Parent: HWND;
  end;

  TBasicWindow = class(TObject)
  private
    FOwner: TObject;
    FHandle: HWND;
	protected
  	procedure CreateInfo(var Info: TCreateInfo); virtual;
    property Owner: TObject read FOwner;
  public
    constructor Create(AOwner: TObject; Parent: HWND = 0; Style: Integer = 0);
    destructor Destroy; override;
    property Handle: HWND read FHandle;
  end;

{ THotWindow }

  THotWindow = class(TBasicWindow)
  protected
		procedure CreateInfo(var Info: TCreateInfo); override;
  end;

{ THotTracker }

	THotTracker = class(TObject)
  private
    FAssociate: HWND;
    FClipped: Boolean;
  	FHotWindow: THotWindow;
    FBlur: Double;
    FBorder: Integer;
    FThickness: Integer;
    FColor: TColor;
    FOpacity: Byte;
    FRadius: Integer;
    FX: Integer;
    FY: Integer;
    FWasVisible: Boolean;
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
    function GetHandle: THandle;
    procedure WMActivateApp(var Msg: TWMActivateApp); message WM_ACTIVATEAPP;
  public
  	constructor Create;
    destructor Destroy; override;
  	procedure Update(NewRect: PRect = nil; NewPoint: PPoint = nil);
    procedure Move(NewPoint: PPoint = nil);
    property Border: Integer read FBorder write FBorder;
    property Thickness: Integer read FThickness write FThickness;
		property Color: TColor read FColor write FColor;
    property Clipped: Boolean read FClipped write FClipped;
    property Blur: Double read FBlur write FBlur;
    property Opacity: Byte read FOpacity write FOpacity;
    property Handle: THandle read GetHandle;
    property Radius: Integer read FRadius write FRadius;
    property Visible: Boolean read GetVisible write SetVisible;
    property Associate: HWND read FAssociate write FAssociate;
    property X: Integer read FX write FX;
    property Y: Integer read FY write FY;
  end;

{ TFocusTracker}

	TFocusTracker = class(THotTracker)
  private
  	FEnabled: Boolean;
    FTranslateKeys: Boolean;
		procedure MessageHook(const Msg: TCWPStruct);
		procedure KeyboardHook(Key: Word; State: Cardinal; var Remove: Boolean);
    procedure SetEnabled(Value: Boolean);
  public
    destructor Destroy; override;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property TranslateKeys: Boolean read FTranslateKeys write FTranslateKeys;
	end;

	PRGBA = ^TRGBA;
	TRGBA = record
    Blue: Byte;
    Green: Byte;
    Red: Byte;
    Alpha: Byte;
  end;

	PRGB = ^TRGB;
  TRGB = record
    Blue: Byte;
    Green: Byte;
    Red: Byte;
  end;

function RGBAToColor(RGBA: TRGBA): TColor;
function ColorToRGBA(Color: TColor): TRGBA;

{ Hooks routines }

type
  TKeyboardHook = procedure(Key: Word; State: Cardinal; var Remove: Boolean) of object;
  TMouseHook = procedure(Msg: Cardinal; const HookStruct: TMouseHookStruct;
    var Remove: Boolean) of object;
  TMessageHook = procedure(const WinProcStruct: TCWPStruct) of object;

procedure HookKeyboard(Hook: TKeyboardHook);
procedure UnhookKeyboard(Hook: TKeyboardHook);
procedure HookMouse(Hook: TMouseHook);
procedure UnhookMouse(Hook: TMouseHook);
procedure HookMessage(Hook: TMessageHook);
procedure UnhookMessage(Hook: TMessageHook);

function FocusTracker: TFocusTracker;

implementation

var
  Black: TRGBA;

const
  WS_EX_LAYERED = $00080000;
  AC_SRC_OVER = $00;
  AC_SRC_ALPHA = $01;
  AC_SRC_NO_PREMULT_ALPHA = $01;
  AC_SRC_NO_ALPHA = $02;
  AC_DST_NO_PREMULT_ALPHA = $10;
  AC_DST_NO_ALPHA = $20;
  LWA_COLORKEY = $00000001;
  LWA_ALPHA = $00000002;
  ULW_COLORKEY = $00000001;
  ULW_ALPHA = $00000002;
  ULW_OPAQUE = $00000004;

type
  PBlendFunction = ^TBlendFunction;
  _BLENDFUNCTION = packed record
    BlendOp: BYTE;
    BlendFlags: BYTE;
    SourceConstantAlpha: BYTE;
    AlphaFormat: BYTE;
  end;
  TBlendFunction = _BLENDFUNCTION;
  BLENDFUNCTION = _BLENDFUNCTION;

function UpdateLayeredWindow(Handle: THandle; hdcDest: HDC; pptDst: PPoint; _psize: PSize;
  hdcSrc: HDC; pptSrc: PPoint; crKey: COLORREF;
  pblend: PBLENDFUNCTION; dwFlags: DWORD): Boolean; stdcall; external user32 name 'UpdateLayeredWindow';

function HeightOf(const Rect: TRect): Integer;
begin
  Result := Rect.Bottom - Rect.Top;
end;

function WidthOf(const Rect: TRect): Integer;
begin
  Result := Rect.Right - Rect.Left;
end;

function IsProcessWindow(Wnd: HWND): Boolean;
var
  Process: THandle;
begin
  Result := IsWindow(Wnd);
  if Result then
  begin
    GetWindowThreadProcessId(Wnd, @Process);
    Result := Process = GetCurrentProcessID;
  end;
end;

{ TAlphaMap }

type
	TAlphaMap = class(TBitmap)
  private
    FOpacity: Byte;
  public
  	constructor Create; override;
    procedure Assign(Source: TPersistent); override;
    procedure CopyMask(Bitmap: TBitmap; Color: TColor);
		procedure CommitOpacity;
    procedure FillMask(Color: TColor);
    procedure Opaque(const Rect: TRect);
    property Opacity: Byte read FOpacity write FOpacity;
  end;

constructor TAlphaMap.Create;
begin
  inherited Create;
	PixelFormat := pf32bit;
  FOpacity := $FF;
end;

procedure TAlphaMap.Assign(Source: TPersistent);
var
	AlphaMap: TAlphaMap absolute Source;
  I: Integer;
begin
	if Source is TAlphaMap then
  begin
  	Opacity := AlphaMap.Opacity;
    for I := 0 to Height - 1 do
    	Move(AlphaMap.ScanLine[I]^, ScanLine[I]^, Width * SizeOf(TRGBA));
  end
  else
	  inherited Assign(Source);
	PixelFormat := pf32bit;
end;

procedure TAlphaMap.CopyMask(Bitmap: TBitmap; Color: TColor);
var
  Fill: TRGBA;
	Swap: Byte;
  A: PRGBA;
  B: PRGB;
  X, Y: Integer;
begin
	if Bitmap.PixelFormat <> pf24Bit then Exit;
	Width := Bitmap.Width;
  Height := Bitmap.Height;
  Fill := TRGBA(ColorToRGB(Color));
  Swap := Fill.Blue;
  Fill.Blue := Fill.Red;
  Fill.Red := Swap;
  for Y := 0 to Height - 1 do
  begin
    A := ScanLine[Y];
    B := Bitmap.Scanline[Y];
    for X := 0 to Width - 1 do
    begin
    	if B.Blue = 0 then
      	A^ := Black
			else if B.Blue > $A0 then
      begin
      	PRGB(A)^ := PRGB(@Fill)^;
				A.Alpha := B.Blue;
			end
      else
      begin
  	  	A.Blue := Round(Fill.Blue * (B.Blue / $FF));
  	  	A.Green := Round(Fill.Green * (B.Green / $FF));
  	  	A.Red := Round(Fill.Red * (B.Red / $FF));
				A.Alpha := B.Blue;
			end;
      Inc(A);
      Inc(B);
    end;
	end;
end;

procedure TAlphaMap.CommitOpacity;
var
	A: PRGBA;
  X, Y: Integer;
begin
  for Y := 0 to Height - 1 do
  begin
    A := ScanLine[Y];
    for X := 0 to Width - 1 do
    begin
    	A.Alpha := FOpacity;
      Inc(A);
		end;
	end;
end;

procedure TAlphaMap.FillMask(Color: TColor);
var
  RGBA: TRGBA;
  RGB: TRGB absolute RGBA;
	Swap: Byte;
  A: PRGBA;
  X, Y: Integer;
begin
  RGBA := TRGBA(ColorToRGB(Color));
  Swap := RGBA.Blue;
  RGBA.Blue := RGBA.Red;
  RGBA.Red := Swap;
  for Y := 0 to Height - 1 do
  begin
    A := ScanLine[Y];
    for X := 0 to Width - 1 do
    begin
    	if A.Alpha > 0 then
				PRGB(A)^ := RGB;
      Inc(A);
    end;
	end;
end;

procedure TAlphaMap.Opaque(const Rect: TRect);
var
	A: PRGBA;
  Row, Col: Integer;
begin
	if (Rect.Left < 0) or (Rect.Top < 0) or (Rect.Right > Width) or
  	(Rect.Bottom > Height) then	Exit;
	for Row := Rect.Top to Rect.Bottom - 1 do
	begin
		A := ScanLine[Row];
    Inc(A, Rect.Left);
    for Col := 0 to WidthOf(Rect) - 1 do
    begin
      A.Alpha := $FF;
    	Inc(A);
    end;
	end;
end;

type
  PRow = ^TRow;
  TRow = array [0..1000000] of TRGB;

  PPRows = ^TPRows;
  TPRows = array [0..1000000] of PRow;

  const
	MaxKernelSize = 100;

type
  TKernelSize = 1..MaxKernelSize;

  TKernel = record
	  Size: TKernelSize;
  	Weights: array[-MaxKernelSize..MaxKernelSize] of single;
  end;

procedure MakeGaussianKernel(var K: TKernel; Radius: Double;
  MaxData, Granularity: Double);
var
	Temp, Delta: Double;
  KernelSize: TKernelSize;
  I: Integer;
begin
  for I := Low(K.Weights) to High(K.Weights) do
  begin
  	Temp := I / Radius;
	  K.Weights[I] := exp(-Temp * Temp / 2);
  end;
  Temp := 0;
  for I := Low(K.Weights) to High(K.Weights) do
	  Temp := Temp + K.Weights[I];
  for I := Low(K.Weights) to High(K.Weights) do
	  K.Weights[I] := K.Weights[I] / Temp;
  KernelSize := MaxKernelSize;
  Delta := Granularity / (2*MaxData);
  Temp := 0;
  while (Temp < Delta) and (KernelSize > 1) do
  begin
	  Temp := Temp + 2 * K.Weights[KernelSize];
  	Dec(KernelSize);
  end;
  K.Size := KernelSize;
  Temp := 0;
  for I := -K.Size to K.Size do
	  Temp := Temp + K.Weights[I];
  for I := -K.Size to K.Size do
	  K.Weights[I] := K.Weights[I] / Temp;
end;

function TrimInt(Lower, Upper, I: Integer): Integer;
begin
	if (I <= Upper) and (I >= Lower) then
  	Result := I
	else if I > Upper then
	  Result := Upper
  else
  	Result := Lower;
end;

function TrimReal(Lower, Upper: Integer; D: Double): Integer;
begin
	if (D < Upper) and (D >= Lower) then
  	Result := Trunc(D)
	else if D > Upper then
		Result := Upper
	else
  	Result := Lower;
end;

procedure BlurRow(var Row: array of TRGB; K: TKernel; P: PRow);
var
  R, G, B: Double;
  W: Double;
  I, J: Integer;
begin
	for I := 0 to High(Row) do
  begin
	  B := 0;
  	G := 0;
	  R := 0;
  	for J := - K.Size to K.Size do
	  begin
  		W := K.Weights[J];
		  with Row[TrimInt(0, High(Row), I - J)] do
		  begin
			  B := B + W * Blue;
			  G := G + W * Green;
			  R := R + W * Red;
		  end;
	  end;
	  with P[I] do
	  begin
		  Blue := TrimReal(0, 255, B);
		  Green := TrimReal(0, 255, G);
		  Red := TrimReal(0, 255, R);
	  end;
  end;
	Move(P[0], Row[0], (High(Row) + 1) * SizeOf(TRGB));
end;

procedure BlurBitmap(Bitmap: TBitmap; const Radius: Double); overload;
var
	Row, Col: Integer;
  Rows: PPRows;
  K: TKernel;
  ACol, P: PRow;
begin
	if Radius < 0.01 then Exit;
	if Bitmap.PixelFormat = pf32Bit then Exit;
	Bitmap.PixelFormat := pf24Bit;
	MakeGaussianKernel(K, Radius, 255, 1);
	GetMem(Rows, Bitmap.Height * SizeOf(PRow));
	GetMem(ACol, Bitmap.Height * SizeOf(TRGB));
	for Row := 0 to Bitmap.Height - 1 do
  	Rows[Row] := Bitmap.Scanline[Row];
	P := AllocMem(Bitmap.Width * SizeOf(TRGB));
	for Row := 0 to Bitmap.Height - 1 do
  	BlurRow(Slice(Rows[Row]^, Bitmap.Width), K, P);
	ReAllocMem(P, Bitmap.Height * SizeOf(TRGB));
	for Col := 0 to Bitmap.Width - 1 do
	begin
  	for Row := 0 to Bitmap.Height - 1 do
		  ACol[Row] := Rows[Row][Col];
	  BlurRow(Slice(ACol^, Bitmap.Height), K, P);
  	for Row := 0 to Bitmap.Height - 1 do
	  	Rows[Row][Col] := ACol[Row];
	end;
	FreeMem(Rows);
	FreeMem(ACol);
	ReAllocMem(P, 0);
end;

procedure BlurBitmap(Bits: PRGB; W, H: Integer; const Radius: Double); overload;
var
	Row, Col: Integer;
  Rows: PPRows;
  K: TKernel;
  ACol, P: PRow;
begin
	if Radius < 0.01 then Exit;
	if (W < 2) or (H < 2) then Exit; 
	MakeGaussianKernel(K, Radius, 255, 1);
	GetMem(Rows, H * SizeOf(PRow));
	GetMem(ACol, H * SizeOf(TRGB));
	for Row := 0 to H - 1 do
  begin
  	Rows[Row] := Pointer(Bits);
    Inc(Bits, W);
	end;
	P := AllocMem(W * SizeOf(TRGB));
	for Row := 0 to H - 1 do
  	BlurRow(Slice(Rows[Row]^, W), K, P);
	ReAllocMem(P, H * SizeOf(TRGB));
	for Col := 0 to W - 1 do
	begin
  	for Row := 0 to H - 1 do
		  ACol[Row] := Rows[Row][Col];
	  BlurRow(Slice(ACol^, H), K, P);
  	for Row := 0 to H - 1 do
	  	Rows[Row][Col] := ACol[Row];
	end;
	FreeMem(Rows);
	FreeMem(ACol);
	ReAllocMem(P, 0);
end;

procedure DrawBlurBubble(Bitmap: TBitmap; W, H, Thickness, Radius: Integer; Blur: Double);
var
	DC: HDC;
  P: HPEN;
  B: HBRUSH;
	I: Integer;
begin
	Bitmap.PixelFormat := pf24bit;
  I := Round(Blur * 4) + 1;
  Bitmap.Width := W + I + Thickness;
  Bitmap.Height := H + I + Thickness;
	DC := Bitmap.Canvas.Handle;
	FillRect(DC, Rect(0, 0, Bitmap.Width, Bitmap.Height), GetStockObject(BLACK_BRUSH));
  if Thickness > 0 then
  begin
    P := SelectObject(DC, CreatePen(PS_SOLID, Thickness, $FFFFFF));
    B := SelectObject(DC, GetStockObject(BLACK_BRUSH));
  end
  else
  begin
    P := SelectObject(DC, GetStockObject(WHITE_PEN));
    B := SelectObject(DC, GetStockObject(WHITE_BRUSH));
  end;
  RoundRect(DC, I shr 1 + Thickness, I shr 1 + Thickness, W + I shr 1 ,
    H + I shr 1, Radius, Radius);
  if Thickness > 0 then
  begin
    SelectObject(DC, B);
    DeleteObject(SelectObject(DC, P));
  end
  else
  begin
    SelectObject(DC, B);
    SelectObject(DC, P);
  end;
  BlurBitmap(Bitmap, Blur);
end;

procedure UpdateAlphaWindow(Wnd: HWND; AlphaMap: TAlphaMap);
var
	Blend: TBlendFunction;
  Rect: TRect;
	P1, P2: TPoint;
  S: TSize;
  DC: HDC;
begin
	if AlphaMap.Height = 0 then Exit;
	SetWindowLong(Wnd, GWL_EXSTYLE, GetWindowLong(Wnd, GWL_EXSTYLE) or WS_EX_LAYERED);
	GetWindowRect(Wnd, Rect);
	P1.X := Rect.Left;
	P1.Y := Rect.Top;
	SetWindowPos(Wnd, 0, 0, 0, AlphaMap.Width, AlphaMap.Height,
		SWP_NOACTIVATE or SWP_NOMOVE);
	with Blend do
	begin
		BlendOp := AC_SRC_OVER;
		BlendFlags := 0;
		SourceConstantAlpha := AlphaMap.Opacity;
		AlphaFormat := AC_SRC_ALPHA;
	end;
	DC := GetDC(0);
	P2 := Point(0, 0);
	S.cx := AlphaMap.Width;
	S.cy := AlphaMap.Height;
	UpdateLayeredWindow(Wnd, DC, @P1, @S, AlphaMap.Canvas.Handle,
		@P2, 0, @Blend, ULW_ALPHA);
	ReleaseDC(0, DC);
end;

{ TBasicWindow }

threadvar
  CreationWindow: TBasicWindow;

function BasicWindowProc(Wnd: HWND; uMsg: Cardinal; wParam: LongInt; lParam: LongInt): Integer; stdcall;
var
  BasicWindowWindow: TBasicWindow;
  Msg: TMessage;
begin
  if CreationWindow <> nil then
  begin
    BasicWindowWindow := CreationWindow;
    BasicWindowWindow.FHandle := Wnd;
    CreationWindow := nil;
    SetWindowLong(Wnd, GWL_USERDATA, Integer(BasicWindowWindow));
  end
  else
    BasicWindowWindow := TBasicWindow(GetWindowLong(Wnd, GWL_USERDATA));
  Result := DefWindowProc(Wnd, uMsg, wParam, lParam);
  if BasicWindowWindow <> nil then
  try
    Msg.Msg := uMsg;
    Msg.wParam := wParam;
    Msg.lParam := lParam;
    Msg.Result := Result;
    BasicWindowWindow.FOwner.Dispatch(Msg);
    if Msg.Msg = WM_DESTROY then
      BasicWindowWindow.FHandle := 0;
  except
    on E: Exception do
      MessageBox(0, PChar(E.ClassName + ': ' + E.Message), 'Error',
        MB_ICONERROR or MB_OK or MB_TASKMODAL);
  end;
end;

procedure TBasicWindow.CreateInfo(var Info: TCreateInfo);
begin
end;

constructor TBasicWindow.Create(AOwner: TObject; Parent: HWND = 0; Style: Integer = 0);
var
  WindowClass: string;
  WindowName: string;
  WndClass: TWndClass;
  Info: TCreateInfo;
begin
  inherited Create;
  FOwner := AOwner;
  WindowClass := ClassName + IntToStr(HInstance);
  WindowName := FOwner.ClassName;
  with WndClass do
  begin
    FillChar(WndClass, SizeOf(TWndClass), #0);
    lpfnWndProc := @BasicWindowProc;
    lpszClassName := PChar(WindowClass);
    hInstance := SysInit.HInstance;
    Windows.RegisterClass(WndClass);
  end;
	FillChar(Info, SizeOf(Info), #0);
  Info.Parent := Parent;
  Info.Style := Style;
	CreateInfo(Info);
  CreationWindow := Self;
  try
  	with Info do
	    CreateWindowEx(ExStyle, PChar(WindowClass), PChar(WindowName), Style,
      	X, Y, W, H, Parent, 0, 0, nil);
  except
    CreationWindow := nil;
  end;
  if FHandle = 0 then
    RaiseLastOSError;
    // switch to this in D5 RaiseLastWin32Error;
end;

destructor TBasicWindow.Destroy;
begin
  if FHandle <> 0 then
  begin
		SetWindowLong(FHandle, GWL_USERDATA, 0);
    DestroyWindow(FHandle);
	end;
end;

{ THotWindow }

procedure THotWindow.CreateInfo(var Info: TCreateInfo);
begin
	inherited CreateInfo(Info);
  if Info.Parent = 0 then
		Info.Style := WS_POPUP or WS_DISABLED
  else
		Info.Style := WS_CHILD or WS_DISABLED;
	Info.ExStyle := WS_EX_TOPMOST	or WS_EX_TOOLWINDOW or WS_EX_TRANSPARENT;
end;

{ THotTracker }

constructor THotTracker.Create;
begin
	inherited Create;
	FHotWindow := THotWindow.Create(Self);
  FColor := clHighlight;
	FBlur := 2;
  FBorder := 15;
	FOpacity := $40;
	FRadius := 15;
end;

destructor THotTracker.Destroy;
begin
  FHotWindow.Free;
  inherited Destroy;
end;

procedure THotTracker.Move(NewPoint: PPoint = nil);
var
	A, B: TRect;
begin
  if not IsWindow(FAssociate) then Exit;
  if NewPoint = nil then
  begin
		GetWindowRect(FAssociate, A);
  	GetWindowRect(FHotWindow.Handle, B);
	  MoveWindow(FHotWindow.Handle, FX + A.Left + Round((WidthOf(A) - WidthOf(B)) / 2),
		  FY + A.Top + Round((HeightOf(A) - HeightOf(B)) / 2), WidthOf(B),
    	HeightOf(B), False);
	end
  else
  	SetWindowPos(FHotWindow.Handle, FAssociate, NewPoint.X, NewPoint.Y, 0, 0,
    	SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
end;

procedure THotTracker.Update(NewRect: PRect = nil; NewPoint: PPoint = nil);
var
	Rect: TRect;
  AlphaMap: TAlphaMap;
  Bitmap: TBitmap;
  W, H: Integer;
  A, B, C: HRGN;
begin
  if not IsWindow(FAssociate) then Exit;
  if NewRect <> nil then
  	Rect := NewRect^
	else
	  GetWindowRect(FAssociate, Rect);
  W := WidthOf(Rect) + FBorder + FThickness;
  H := HeightOf(Rect) + FBorder + FThickness;
  AlphaMap := TAlphaMap.Create;
  try
		Bitmap := TBitmap.Create;
  	try
  		DrawBlurBubble(Bitmap, W, H, FThickness, FRadius, FBlur);
      AlphaMap.CopyMask(Bitmap, FColor);
      W := AlphaMap.Width;
      H := AlphaMap.Height;
	  finally
  		Bitmap.Free;
		end;
    AlphaMap.Opacity := FOpacity;
		UpdateAlphaWindow(FHotWindow.Handle, AlphaMap);
	finally
		AlphaMap.Free;
	end;
  if FClipped then
  begin
  	A := CreateRectRgn(0, 0, WidthOf(Rect), HeightOf(Rect));
    B := CreateRectRgn(0, 0, W, H);
    W := (W - WidthOf(Rect));
    if Odd(W) then Inc(W);
    H := (H - HeightOf(Rect));
    if Odd(H) then Inc(H);
    OffsetRgn(A, W div 2 - FX, H div 2 - FY);
  	C := CreateRectRgn(0, 0, 1, 1);
    CombineRgn(C, A, B, RGN_XOR);
    SetWindowRgn(FHotWindow.Handle, C, False);
    DeleteObject(A);
    DeleteObject(B);
  end;
  Move(NewPoint);
	Visible := True;
end;


function THotTracker.GetHandle: THandle;
begin
	Result := FHotWindow.Handle;
end;

function THotTracker.GetVisible: Boolean;
begin
	Result := IsWindowVisible(FHotWindow.Handle);
end;

procedure THotTracker.SetVisible(const Value: Boolean);
begin
	if Value <> Visible then
		if Value then
  		ShowWindow(FHotWindow.Handle, SW_SHOWNOACTIVATE)
		else
			ShowWindow(FHotWindow.Handle, SW_HIDE);
end;

procedure THotTracker.WMActivateApp(var Msg: TWMActivateApp);
begin
	inherited;
  if Msg.Active then
  	if FWasVisible then
    	Visible := True
		else
	else
  begin
  	FWasVisible := Visible;
    Visible := False;
  end;
end;

{ TFocusTracker }

destructor TFocusTracker.Destroy;
begin
	Enabled := False;
  inherited Destroy;
end;

procedure TFocusTracker.KeyboardHook(Key: Word; State: Cardinal;
  var Remove: Boolean);
begin
	if not FTranslateKeys then Exit;
	if (State and  $C0000000 = $C0000000) and (Key = VK_RETURN) then
  begin
		keybd_event(VK_TAB, 0, 0, 0);
		keybd_event(VK_TAB, 0, KEYEVENTF_KEYUP, 0);
  end;
end;

procedure TFocusTracker.MessageHook(const Msg: TCWPStruct);
begin
	case Msg.message of
    WM_SETFOCUS:
	    if IsProcessWindow(Msg.hwnd) then
  	  begin
    		Associate := Msg.hwnd;
				Update;
	    end;
    WM_WINDOWPOSCHANGING:
    	if IsChild(Msg.hwnd, Associate) then
      	Move;
    WM_KILLFOCUS:
      Visible := IsProcessWindow(Msg.wParam);
		WM_SIZE:
    	if Msg.hwnd = Associate then
      	Update;
  end;
end;

procedure TFocusTracker.SetEnabled(Value: Boolean);
begin
	if Value <> FEnabled then
  begin
  	FEnabled := Value;
    if FEnabled then
    begin
		  HookMessage(MessageHook);
		  HookKeyboard(KeyboardHook);
    end
    else
    begin
			UnhookMessage(MessageHook);
		  UnhookKeyboard(KeyboardHook);
      Visible := False;
    end;
	end;
end;

procedure LoadAlphaWindow(Wnd: HWND; const FileName: string; Opacity: Byte = $FF);
var
	AlphaMap: TAlphaMap;
begin
	AlphaMap := TAlphaMap.Create;
  try
  	AlphaMap.LoadFromFile(FileName);
		AlphaMap.Opacity := Opacity;
    UpdateAlphaWindow(Wnd, AlphaMap);
  finally
  	AlphaMap.Free;
  end;
end;

{ Color conversion } 

function RGBAToColor(RGBA: TRGBA): TColor;
begin
	RGBA.Alpha := RGBA.Blue;
  RGBA.Blue := RGBA.Red;
  RGBA.Red := RGBA.Alpha;
  RGBA.Alpha := 0;
  Result := TColor(RGBA);
end;

function ColorToRGBA(Color: TColor): TRGBA;
begin
  Result := TRGBA(ColorToRGB(Color));
	Result.Alpha := Result.Blue;
  Result.Blue := Result.Red;
  Result.Red := Result.Alpha;
  Result.Alpha := 0;
end;

{ Hook support routines }

type
  PMethod = ^TMethod;
  THookList = record
    Hook: HHOOK;
    Callbacks: TList;
  end;

procedure SetHook(var HookList: THookList; Kind: Integer; HookProc: TFarProc;
  const Method: TMethod);
var
  DynamicMethod: PMethod;
begin
  with HookList do
    if Hook = 0 then
    begin
      Hook := SetWindowsHookEx(Kind, HookProc, 0, GetCurrentThreadId);
      Callbacks := TList.Create;
    end;
  New(DynamicMethod);
  DynamicMethod^ := Method;
  HookList.Callbacks.Add(DynamicMethod);
end;

procedure ReleaseHook(var HookList: THookList; const Method: TMethod);
var
  DyanmicMethod: PMethod;
  I: Integer;
begin
  with HookList do
    if Hook <> 0 then
    begin
      for I := 0 to Callbacks.Count do
      begin
        DyanmicMethod := Callbacks[I];
        if (DyanmicMethod.Code = Method.Code) and
          (DyanmicMethod.Data = Method.Data) then
        begin
          Dispose(DyanmicMethod);
          Callbacks.Delete(I);
          Break;
        end;
      end;
      if Callbacks.Count = 0 then
      begin
        UnhookWindowsHookEx(Hook);
        Hook := 0;
        Callbacks.Free;
        Callbacks := nil;
      end;
    end
end;

var
  InternalKeyboardHooks: THookList;

function KeyboardHook(Code: Integer; wParam: LongInt; lParam: LongInt): LongInt; stdcall;
var
  Remove: Boolean;
  Method: TMethod;
  Callback: TKeyboardHook absolute Method;
  I: Integer;
begin
  with InternalKeyboardHooks do
    if Code < 0 then
      Result := CallNextHookEx(Hook, Code, wParam, lParam)
    else
    begin
      Remove := False;
      for I := 0 to Callbacks.Count - 1 do
      begin
        Method := PMethod(Callbacks[I])^;
        Callback(wParam, lParam, Remove);
      end;
      if Remove then Result := 1 else Result := 0;
    end;
end;

procedure HookKeyboard(Hook: TKeyboardHook);
var
  Method: TMethod absolute Hook;
begin
  SetHook(InternalKeyboardHooks, WH_KEYBOARD, @KeyboardHook, Method);
end;

procedure UnhookKeyboard(Hook: TKeyboardHook);
var
  Method: TMethod absolute Hook;
begin
  ReleaseHook(InternalKeyboardHooks, Method);
end;

var
  InternalMouseHooks: THookList;

function MouseHook(Code: Integer; Msg: Cardinal;
  HookStruct: PMouseHookStruct): Integer; stdcall;
var
  Remove: Boolean;
  Method: TMethod;
  Callback: TMouseHook absolute Method;
  I: Integer;
begin
  with InternalMouseHooks do
    if Code < 0 then
      Result := CallNextHookEx(Hook, Code, Msg, Integer(HookStruct))
    else
    begin
      Remove := False;
      for I := 0 to Callbacks.Count - 1 do
      begin
        Method := PMethod(Callbacks[I])^;
        Callback(Msg, HookStruct^, Remove);
      end;
      if Remove then Result := 1 else Result := 0;
    end;
end;

procedure HookMouse(Hook: TMouseHook);
var
  MethodParam: TMethod absolute Hook;
  Method: PMethod;
begin
  with InternalMouseHooks do
    if Hook = 0 then
    begin
      Hook := SetWindowsHookEx(WH_MOUSE, @MouseHook, 0, GetCurrentThreadId);
      Callbacks := TList.Create;
    end;
  New(Method);
  Method^ := MethodParam;
  InternalMouseHooks.Callbacks.Add(Method);
end;

procedure UnhookMouse(Hook: TMouseHook);
var
  MethodParam: TMethod absolute Hook;
  Method: PMethod;
  I: Integer;
begin
  with InternalMouseHooks do
    if Hook <> 0 then
    begin
      for I := 0 to Callbacks.Count do
      begin
        Method := Callbacks[I];
        if (Method.Code = MethodParam.Code) and
          (Method.Data = MethodParam.Data) then
        begin
          Dispose(Method);
          Callbacks.Delete(I);
          Break;
        end;
      end;
      if Callbacks.Count = 0 then
      begin
        UnhookWindowsHookEx(Hook);
        Hook := 0;
        Callbacks.Free;
        Callbacks := nil;
      end;
    end;
end;

var
  InternalMessageHooks: THookList;

function MessageHook(Code: Integer; CurrentProcess: Cardinal;
  HookStruct: PCWPStruct): Integer; stdcall;
var
  Method: TMethod;
  Callback: TMessageHook absolute Method;
  I: Integer;
begin
  with InternalMessageHooks do
    if Code < 0 then
      Result := CallNextHookEx(Hook, Code, CurrentProcess, Integer(HookStruct))
    else
    begin
			for I := 0 to Callbacks.Count - 1 do
      begin
    		Method := PMethod(Callbacks[I])^;
      	Callback(HookStruct^);
      end;
      Result := 0;
    end;
end;

procedure HookMessage(Hook: TMessageHook);
var
  MethodParam: TMethod absolute Hook;
  Method: PMethod;
begin
  with InternalMessageHooks do
    if Hook = 0 then
    begin
      Hook := SetWindowsHookEx(WH_CALLWNDPROC, @MessageHook, 0, GetCurrentThreadId);
      Callbacks := TList.Create;
    end;
  New(Method);
  Method^ := MethodParam;
  InternalMessageHooks.Callbacks.Add(Method);
end;

procedure UnhookMessage(Hook: TMessageHook);
var
  MethodParam: TMethod absolute Hook;
  Method: PMethod;
  I: Integer;
begin
  with InternalMessageHooks do
    if Hook <> 0 then
    begin
      for I := 0 to Callbacks.Count do
      begin
        Method := Callbacks[I];
        if (Method.Code = MethodParam.Code) and
          (Method.Data = MethodParam.Data) then
        begin
          Dispose(Method);
          Callbacks.Delete(I);
          Break;
        end;
      end;
      if Callbacks.Count = 0 then
      begin
        UnhookWindowsHookEx(Hook);
        Hook := 0;
        Callbacks.Free;
        Callbacks := nil;
      end;
    end;
end;

procedure ReleaseAllHooks;

  procedure ReleaseHooks(var Hooks: THookList);
  var
    I: Integer;
  begin
	  with Hooks do
  	  if Hook <> 0 then
    	begin
	      UnhookWindowsHookEx(Hook);
  	    Hook := 0;
    	  for I := 0 to Callbacks.Count - 1 do
        Dispose(Callbacks[I]);
      	Callbacks.Free;
	      Callbacks := nil;
  	  end;
  end;

begin
  ReleaseHooks(InternalKeyboardHooks);
  ReleaseHooks(InternalMouseHooks);
  ReleaseHooks(InternalMessageHooks);
end;

var
	InternalFocusTracker: TObject;

function FocusTracker: TFocusTracker;
begin
  if InternalFocusTracker = nil then
 		InternalFocusTracker := TFocusTracker.Create;
	Result := TFocusTracker(InternalFocusTracker);
end;

initialization
  InternalMouseHooks.Hook := 0;
	InternalFocusTracker := nil;
finalization
	InternalFocusTracker.Free;
  ReleaseAllHooks;
end.
