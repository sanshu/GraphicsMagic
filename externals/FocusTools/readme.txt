To use focus tracker simply add "FocusTools" to your uses clause then add code to adjust its settings:

  FocusTracker.Enabled := True;
  FocusTracker.Color := clHighlight;
  FocusTracker.Opacity := $7F;
  FocusTracker.Blur := 2;
  FocusTracker.Border := 2;
  FocusTracker.Radius := 12;
  FocusTracker.Thickness := 3;

After you have the properties you want, call:

  FocusTracker.Update;

Which forces FocusTracker to update changes. That's all there is to it.