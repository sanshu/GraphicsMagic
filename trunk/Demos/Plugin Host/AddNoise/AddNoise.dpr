{ This is a plug-in filters specifically designed for GraphicsMagic.
  Copyright (C) 2008 Ma Xiaoguang & Ma Xiaoming < gmbros@hotmail.com >,
  all rights reserved. }

library AddNoise;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

{ This include file is suggested by Andre Felix Miertschink to
  make this program could be compiled by Delphi XE. }

{$I ..\..\..\GraphicsMagic.inc}

uses
  FastCode,
  FastMove,
  VCLFixPack,
  RtlVclOptimize,
  SysUtils,
  Classes,
  Forms,
  Controls,
  GR32,
  StrUtils,
  AddNoiseDlg in 'AddNoiseDlg.pas' {frmAddNoise},
  gmTypes in '..\..\..\GraphicsMagicLib\gmTypes.pas',
  gmPluginFuncs in '..\..\..\GraphicsMagicLib\gmPluginFuncs.pas',
  gmFilterBridges in '..\parser\gmFilterBridges.pas';

{$E gmp}

{$R *.res}

{$WARN UNSAFE_TYPE OFF}

type
  TUpdateViewProc = procedure;

var
  SelectedChannelSet: TgmChannelSet;

resourcestring

mockup = '[Add Noise]'#13#10+
'	Amount:'#13#10+
'	[<0~400>]'#13#10+
'	[x] Monochromatic'#13#10;

function GetPluginMockup: PChar; stdcall;
begin
  Result := PChar(mockup);
end;

function GetPluginCategory: PChar; stdcall;
begin
  Result := 'Noise';
end;

function GetPluginName: PChar; stdcall;
begin
  Result := 'Add Noise';
end;

function IsAdjustablePlugin: Boolean; stdcall;
begin
  Result := True;
end;

// indicates whether this plugin supports single channel operations
function IsChannelSupported: Boolean; stdcall;
begin
  Result := True;
end;

// set selected channels to process
function SetChannels(const ChannelSet: TgmChannelSet): Boolean; stdcall;
begin
  SelectedChannelSet := ChannelSet;
  Result             := True;
end;

function ExecutePlugin(AppHandle: THandle; DestBmpPtr: PColor32;
  const Width, Height: Integer; UpdateViewProc: TUpdateViewProc;
  const BKColor: TColor32 = $00000000): Boolean; stdcall;
var
  LOldAppHandle: THandle;
  LIsSuccessed : Boolean;
begin
  LIsSuccessed  := True;
  LOldAppHandle := Application.Handle;

  Application.Handle := AppHandle;
  frmAddNoise        := TfrmAddNoise.Create(Application);  // create and open the setup dialog of this filter
  try
    try
      frmAddNoise.FDestBmpPtr     := DestBmpPtr;
      frmAddNoise.FUpdateViewProc := UpdateViewProc;
      frmAddNoise.FChannelSet     := SelectedChannelSet;
      CopyBmpDataFromPtr(DestBmpPtr, Width, Height, frmAddNoise.FSourceBmp);

      if frmAddNoise.ShowModal = mrOK then
      begin
        if not frmAddNoise.chckbxPreview.Checked then
        begin
          CopyBmpDataToPtr(frmAddNoise.FProcessedBmp, DestBmpPtr, Width, Height);

          if Assigned(UpdateViewProc) then
          begin
            UpdateViewProc;
          end;
        end;
      end
      else
      begin
        CopyBmpDataToPtr(frmAddNoise.FSourceBmp, DestBmpPtr, Width, Height);

        if Assigned(UpdateViewProc) then
        begin
          UpdateViewProc;
        end;

        LIsSuccessed := False;
      end;

    except
      LIsSuccessed := False;
    end;

  finally
    frmAddNoise.Free;
    Application.Handle := LOldAppHandle;
  end;
  
  Result := LIsSuccessed;
end; 

//------------------------------------------------------------------------------

function ExecuteFilter(AppHandle: THandle; DestBmpPtr: PColor32;
  const Width, Height: Integer; //UpdateViewProc: TUpdateViewProc;
  ParamsStr: PChar;
  const BKColor: TColor32 = $00000000 ): Boolean; stdcall;
var
  LOldAppHandle: THandle;
  LIsSuccessed : Boolean;
  //FSourceBmp : TBitmap32;
begin
  LIsSuccessed  := True;
  LOldAppHandle := Application.Handle;

  Application.Handle := AppHandle;



  //--------start
  //FSourceBmp := TBitmap32.Create;

  //frmAddNoise        := TfrmAddNoise.Create(Application);  // create and open the setup dialog of this filter
  try
  //CopyBmpDataFromPtr(DestBmpPtr, Width, Height, FSourceBmp);

  AddNoiseExecuteAddNoise(//FSourceBmp,
    SelectedChannelSet, //FChannelSet: TgmChannelSet;
    //UpdateViewProc, //FUpdateViewProc: TUpdateViewProc;
    DestBmpPtr, //FDestBmpPtr: PColor32;
    Width, Height,
    ParamsStr);

    {
    try
      frmAddNoise.FDestBmpPtr     := DestBmpPtr;
      frmAddNoise.FUpdateViewProc := UpdateViewProc;
      frmAddNoise.FChannelSet     := SelectedChannelSet;
      CopyBmpDataFromPtr(DestBmpPtr, Width, Height, frmAddNoise.FSourceBmp);

      if frmAddNoise.ShowModal = mrOK then
      begin
        if not frmAddNoise.chckbxPreview.Checked then
        begin
          CopyBmpDataToPtr(frmAddNoise.FProcessedBmp, DestBmpPtr, Width, Height);

          if Assigned(UpdateViewProc) then
          begin
            UpdateViewProc;
          end;
        end;
      end
      else
      begin
        CopyBmpDataToPtr(frmAddNoise.FSourceBmp, DestBmpPtr, Width, Height);

        if Assigned(UpdateViewProc) then
        begin
          UpdateViewProc;
        end;

        LIsSuccessed := False;
      end;

    except
      LIsSuccessed := False;
    end;
     }
  finally
    //frmAddNoise.Free;
    Application.Handle := LOldAppHandle;
    //FSourceBmp.Free;
  end;

  Result := LIsSuccessed;
end;


exports
  ExecutePlugin,
  ExecuteFilter, GetPluginMockup,
  GetPluginCategory,
  GetPluginName,
  IsAdjustablePlugin,
  IsChannelSupported,
  SetChannels;

begin
  // initial settings
  SelectedChannelSet := [csRed, csGreen, csBlue];
end.
