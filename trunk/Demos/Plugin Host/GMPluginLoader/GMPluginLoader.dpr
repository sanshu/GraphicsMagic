{ This program is for testing how to load/run the .gmp plugins which
  specifically designed for GraphicsMagic.

  CopyRight(C) 2001-2008, Ma Xiaoguang & Ma Xiaoming < gmbros@hotmail.com >.
  All rights reserved. }

program GMPluginLoader;

{ This include file is suggested by Andre Felix Miertschink to
  make this program could be compiled by Delphi XE. }

{$I ..\..\GraphicsMagic.inc}

uses
  FastCode,
  FastMove,
  VCLFixPack,
  RtlVclOptimize,
  Forms,
  Main in 'Main.pas' {frmMain},
  gmFilterBridges in '..\parser\gmFilterBridges.pas',
  uParser in '..\parser\uParser.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
