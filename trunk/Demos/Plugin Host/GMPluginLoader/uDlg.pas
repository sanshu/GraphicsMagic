unit uDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  GR32, gmFilterBridges, uParser,
  main, StdCtrls, Buttons, ExtCtrls;

type
  TfrmDlg = class(TForm)
    pnl1: TPanel;
    btbtnOK: TBitBtn;
    btbtnCancel: TBitBtn;
    chckbxPreview: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure chckbxPreviewClick(Sender: TObject);
  private
    { Private declarations }
    FSourceBmp    : TBitmap32;
    FTargetBmp    : TBitmap32;
    PTarget       : PColor32; // the fact to be updated

  public
    { Public declarations }
    FilterFunc : TgmFilterFunc;
    Bmp32 : TBitmap32;
    Params : TgmFilterBridge;
    procedure DoParamsChanged(Sender : TObject);
    function Execute(AMockup: string; Target:TBitmap32):Integer;
  end;

var
  frmDlg: TfrmDlg;

implementation
uses AddTheNoiseTest, gmPluginFuncs;

{$R *.dfm}

{procedure UpdateView;
var
  //BmpLayer: TBitmapLayer;
  i : Integer;
  u,z : Integer;
begin
  //BmpLayer := TBitmapLayer(frmdlg.);

  // Do everything we want at here...

  //BmpLayer.Bitmap.Changed;
  i := 10;
  //u := i * 100;
  for u := 0 to i do
     z := u +i;
end;}

{ TfrmDlg }

procedure TfrmDlg.DoParamsChanged(Sender: TObject);
var
//P         : PColor32;
{  TgmFilterFunc = function(AppHandle: THandle; DestBmpPtr: PColor32;
    const Width, Height: Integer; UpdateViewProc: TUpdateViewProc;
    ParamsStr: PChar;
    const BKColor: TColor32 = $00000000): Boolean; stdcall;
}
s : string;
c : pchar;
b : Boolean;
begin
//
  //FProcessedBmp.Assign(FSourceBmp);
  //CopyBmpDataFromPtr(PTarget, FSourceBmp.Width, FSourceBmp.Height, FSourceBmp);
  CopyBmpDataToPtr(FSourceBmp, PTarget, FSourceBmp.Width, FSourceBmp.Height);

  //P  := @Bmp32.Bits[0];

//  pm.RunPlugin(P, Bmp32.Width, Bmp32.Height, nil);
  s := Params.AsString+#0;
  c := PChar(s);
  //EXTERNAL FUNC
  b := FilterFunc(Application.Handle,PTarget, FSourceBmp.Width, FSourceBmp.Height,  {@UpdateView,} c,0 );
  //INTERNAL FUNC, FOR DEBUG
  //b := ExecuteFilter(Application.Handle, PTarget, FSourceBmp.Width, FSourceBmp.Height,  c,0 );

{ TODO : add a properly selected_channels here, if not RGB_Channels were selected }
  if self.chckbxPreview.Checked then
    FTargetBmp.Changed;
end;

function TfrmDlg.Execute(AMockup: string; Target: TBitmap32): Integer;
var LBmp : TBitmap32;
begin
{ TODO : Add the loading library.dll.gmp here, everything related to the a fillter called must be here, not elsewhere. }

  //  LBmp := TBitmap32.Create;
  FTargetBmp := Target;
  PTarget  := @Target.Bits[0];

  FSourceBmp.Assign(Target);

  Params := Parse(amockup, self);

  //frmDlg.Params := LParams;
  Params.OnChange := DoParamsChanged;
  Result := ShowModal;

  { TODO : do sendback to original (before affected) here, when modalresult <> oke }  
end;

procedure TfrmDlg.FormCreate(Sender: TObject);
begin
  FSourceBmp    := TBitmap32.Create;
  //FTargetBmp := TBitmap32.Create;
end;

procedure TfrmDlg.FormDestroy(Sender: TObject);
begin
  FSourceBmp.Free;
end;

procedure TfrmDlg.chckbxPreviewClick(Sender: TObject);
begin
  if chckbxPreview.Checked then
  begin
     DoParamsChanged(Self);
  end
  else
  begin
    FTargetBmp.Assign(FSourceBmp);
  end;  
end;

end.
