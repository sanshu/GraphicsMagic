unit uParser;

interface
uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, gmFilterBridges;


function Parse(script : string; AParent : TWinControl): TgmFilterBridge;

implementation
uses Math;

function CleanCaption(ACaption: string): string;
var i :integer;
begin
  Result := Trim(StringReplace(ACaption, ' ', '_', [rfReplaceAll]));
  for i := Length(Result) downto 1 do
    if not (Result[i] in ['a'..'z', 'A'..'Z', '0'..'9', '_'] ) then
      Delete(Result, i, 1); 

end;


function Parse(script : string; AParent : TWinControl): TgmFilterBridge;
var L,R : TStringList;
  i,k,x,y,h,m : Integer;
  ch : Char;
  C : TControl;
  s,s1,s2  : string;
  Last_Label : string;
  Form : TCustomForm;
  Params : TgmFilterBridge;
  par    : TgmFilterParam;
begin
  Form := GetParentForm(AParent);
  Params := TgmFilterBridge.Create;
  Params.Duplicates := dupError;
  y := 10;
  L := TStringList.Create;
  R := TStringList.Create;
  L.Text := script;
  for i := 0 to L.Count -1 do
  begin
    R.Text := Trim(StringReplace(L[i], '  ', #13#10, [rfReplaceAll])); //get one line script
    x := 25;
    h := 0;
    for k := 0 to R.Count -1 do
    begin
      s := R[k];
      if Trim(R[k]) = '' then Continue;

      if i = 0 then //CAPTION OF FORM
      {with TPanel.Create(Form) do
      begin
        Parent := AParent;
        Caption := R[k];
        Left := x;
        Top := y;
        Align := alTop;
        Height := 20;
        Color := clBlue;
        Font.Color := clWhite;
        //inc(y,Height+4);
        //Inc(x,Width+3);

        h := max(h,Height+4);
      end}
      begin
        form.Caption := R[k];
      end
      else
      if R[k][1] = '[' then
      begin
        ch := R[k][2];
        case ch of
          'x',' ' :
              begin
                C := TCheckBox.Create(Form);
                with TCheckBox(C) do
                begin
                  Parent := AParent;
                  Caption := Copy(R[k],4,255);
                  Left := x;
                  Top := y;
                  //inc(y,Height+4);
                  Inc(x,Width+3);
                  h := max(h,Height+4);
                  Last_Label := CleanCaption(Trim(caption));
                  par := Params.Add( Last_Label,C, varBoolean );
                  OnClick := par.OnChange;
                  Checked := Copy(R[k],2,1) = 'x';
                end;

              end;
          '$' :
              begin
                C := TEdit.Create(Form);
                with TEdit(C) do
                begin
                  Parent := AParent;
                  Left := x;
                  Top := y;
                  Inc(x,Width+3);
                  h := max(h,Height+4);
                  par := Params.Add(Last_Label,C);
                  OnChange := par.OnChange;
                end;

              end;
          '<' :
              begin
                if Pos('~',s) > 0 then
                begin
                  C := TTrackBar.Create(Form);
                  with TTrackBar(C) do
                  begin
                    Left := x;
                    Top := y;
                    Parent := AParent;
                    ThumbLength := 15;
                    s1 := Copy(R[k],3,255); //[<
                    if Pos('@',s1) > 0 then
                      s2 := Copy(s1,1,  Pos('@',s1)-2  )
                    else
                      s2 := Copy(s1,1,  Length(s1)-2  );
                    m := Pos('~',s2);

                    Min := StrToInt(Copy(s2,1,m-1));
                    Max := StrToInt(Copy(s2,m+1,255));
                    Height := 25;
                    //Checked := Copy(R[k],2,1) = 'x';
                    //Invalidate;
                    //inc(y,Height+4);
                    Inc(x,Width+3);
                    h := Math.Max(h,Height+4);
                    par := Params.Add(Last_Label,C);
                    OnChange := par.OnChange;
                  end;

                end;
              end;
          '_' :
              begin
                if Pos('___|v]',s) > 0 then
                begin
                  C := TComboBox.Create(Form);
                  with C do
                  begin
                    Left := x;
                    Top := y;
                    Parent := AParent;
                    //inc(y,Height+4);
                    Inc(x,Width+3);
                    h := Math.Max(h,Height+4);
                  end;
                  Params.Add(Last_Label,C);
                end;
              end;
        end;


      end
      else
      if R[k] = '$' then //label only, perhap put after trackbar
      begin
        C := TLabel.Create(Form);
        with TLabel(C) do
        begin
          Parent := AParent;
          Caption := '';
          Left := x;
          Top := y;
          //inc(y,Height+4);
          Inc(x,Width+3);
          h := max(h,Height+4);
          //Last_Label := CleanCaption(Trim(caption));
          Params.Add(Last_Label,C);
        end;
      end
      else
      if R[k][1] = '(' then
      begin
        C := TRadioButton.Create(Form);
        with TRadioButton(C) do
        begin
          Parent := AParent;
          Caption := Copy(R[k],4,255);
          Left := x;
          Top := y;
          //inc(y,Height+4);
          Inc(x,Width+3);
          h := max(h,Height+4);
          Last_Label := CleanCaption(Trim(caption));
          par := Params.Add( Last_Label,C, varBoolean );
          par.IsStored := False;
          OnClick := par.OnChange;
          Checked := Copy(R[k],2,1) = '*';
        end;
      end
      else
      with TLabel.Create(Form) do   //used as name of later editable widget
      begin
        Parent := AParent;
        Caption := R[k];
        Left := x;
        Top := y;
        //inc(y,Height+4);
        Inc(x,Width+3);
        h := max(h,Height+4);

        Last_Label := CleanCaption(Trim(caption));
      end;
    end;
    inc(y,h);
  end;
  Result := Params;
end;

  
end.
